		.text
		.align  2
		.globl  _start

_start:
		addi x5, x0, 1
		nop
		nop
		nop
		nop
loop:
		addi x5, x5, 2
		nop
		nop
		nop
		slli x5, x5, 1
		nop
		nop
		nop
		sw x5, 16(x0)
		nop
		nop
		nop
		lw x5, 16(x0)
		nop
		nop
		nop
		lh x6, 16(x0)
		nop
		nop
		nop
		jal x0, loop
		nop
		nop
		nop

		.end _start
		.size _start, .-_start
