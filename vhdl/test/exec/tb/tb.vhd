library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_textio.all;

library std; -- for Printing
use std.textio.all;

use work.mem_pkg.all;
use work.op_pkg.all;
use work.core_pkg.all;
use work.tb_util_pkg.all;

entity tb is
end entity;

architecture bench of tb is

	constant CLK_PERIOD : time := 10 ns;

	signal clk : std_logic;
	signal res_n : std_logic := '0';

	file input_file : text;
	file output_ref_file : text;


	type INPUT is
		record
			stall : std_logic;
			op	: exec_op_type;
				-- result.op.aluop := str_to_alu_op(l.all);
				-- result.op.alusrc1 := str_to_sl(l(1));
				-- result.op.alusrc2 := str_to_sl(l(1));
				-- result.op.alusrc3 := str_to_sl(l(1));
				-- result.op.rs1 := bin_to_slv(l.all, ADDR_WIDTH);
				-- result.op.rs2 := bin_to_slv(l.all, ADDR_WIDTH);
				-- result.op.readdata1 := bin_to_slv(l.all, DATA_WIDTH);
				-- result.op.readdata2 := bin_to_slv(l.all, DATA_WIDTH);
				-- result.op.imm := bin_to_slv(l.all, DATA_WIDTH);
			pc_in : pc_type;
			memop_in : mem_op_type;
			wbop_in : wb_op_type;
		end record;

	type OUTPUT is
		record
			pc_old_out : pc_type;
			pc_new_out : pc_type;
			aluresult : data_type;
			wrdata : data_type;
			zero : std_logic;
			memop_out : mem_op_type;
			wbop_out : wb_op_type;
		end record;

	signal inp  : INPUT := (
		'0',
		EXEC_NOP,
		(others => '0'),
		MEM_NOP,
		WB_NOP
	);

	signal outp : OUTPUT;


	impure function read_next_input(file f : text) return INPUT is
		variable l : line;
		variable result : INPUT;
	begin
		l := get_next_valid_line(f);
		result.stall := str_to_sl(l(1));

		-- exec_op>>>>>
		l := get_next_valid_line(f);
		result.op.aluop := str_to_alu_op(l.all);

		l := get_next_valid_line(f);
		result.op.alusrc1 := str_to_sl(l(1));

		l := get_next_valid_line(f);
		result.op.alusrc2 := str_to_sl(l(1));

		l := get_next_valid_line(f);
		result.op.alusrc3 := str_to_sl(l(1));

		l := get_next_valid_line(f);
		result.op.rs1 := bin_to_slv(l.all, REG_BITS);

		l := get_next_valid_line(f);
		result.op.rs2 := bin_to_slv(l.all, REG_BITS);

		l := get_next_valid_line(f);
		result.op.readdata1 := bin_to_slv(l.all, DATA_WIDTH);

		l := get_next_valid_line(f);
		result.op.readdata2 := bin_to_slv(l.all, DATA_WIDTH);

		l := get_next_valid_line(f);
		result.op.imm := bin_to_slv(l.all, DATA_WIDTH);
		--<<<<<

		l := get_next_valid_line(f);
		result.pc_in := bin_to_slv(l.all, PC_WIDTH);

		return result;
	end function;

	impure function read_next_output(file f : text) return OUTPUT is
		variable l : line;
		variable result : OUTPUT;
	begin
		result.memop_out := inp.memop_in;
		result.wbop_out := inp.wbop_in;



		l := get_next_valid_line(f);
		result.pc_old_out := bin_to_slv(l.all, PC_WIDTH);

		l := get_next_valid_line(f);
		result.pc_new_out := bin_to_slv(l.all, PC_WIDTH);

		l := get_next_valid_line(f);
		result.aluresult := bin_to_slv(l.all, DATA_WIDTH);

		l := get_next_valid_line(f);
		result.wrdata := bin_to_slv(l.all, DATA_WIDTH);

		l := get_next_valid_line(f);
		result.zero := str_to_sl(l(1));

		return result;
	end function;

	procedure check_output(output_ref : OUTPUT) is
		variable passed : boolean;
	begin
		passed :=
			(outp.pc_old_out = output_ref.pc_old_out) and
			(outp.pc_new_out = output_ref.pc_new_out) and
			(outp.aluresult = output_ref.aluresult) and
			(outp.wrdata = output_ref.wrdata) and
			(outp.zero = output_ref.zero);

		if passed then
			report " PASSED: "
			& "op=" & to_string(inp.op.aluop) &
				" " & to_string(inp.op.alusrc1) &
				" " & to_string(inp.op.alusrc2) &
				" " & to_string(inp.op.alusrc3) &
				" " & slv_to_bin(inp.op.rs2) &
				" " & slv_to_bin(inp.op.rs1) &
				" " & slv_to_bin(inp.op.readdata1) &
				" " & slv_to_bin(inp.op.readdata2) &
				" " & slv_to_bin(inp.op.imm)
			& lf
			& " A=" & slv_to_bin(inp.pc_in)
			& lf
			severity note;
		else
			report "FAILED: "
			& "op=" & to_string(inp.op.aluop) &
				" " & to_string(inp.op.alusrc1) &
				" " & to_string(inp.op.alusrc2) &
				" " & to_string(inp.op.alusrc3) &
				" " & slv_to_bin(inp.op.rs2) &
				" " & slv_to_bin(inp.op.rs1) &
				" " & slv_to_bin(inp.op.readdata1) &
				" " & slv_to_bin(inp.op.readdata2) &
				" " & slv_to_bin(inp.op.imm)
			& lf
			& " pc_in=" & slv_to_bin(inp.pc_in)
			& lf
			& lf
			& "**      expected:"
			& " pc_old_out=" & slv_to_bin(output_ref.pc_old_out)
			& " pc_new_out=" & slv_to_bin(output_ref.pc_new_out)
			& " aluresult=" & slv_to_bin(output_ref.aluresult)
			& " wrdata=" & slv_to_bin(output_ref.wrdata)
			& " zero=" & to_string(output_ref.zero)
			& lf
			& "**        actual:"
			& " pc_old_out=" & slv_to_bin(outp.pc_old_out)
			& " pc_new_out=" & slv_to_bin(outp.pc_new_out)
			& " aluresult=" & slv_to_bin(outp.aluresult)
			& " wrdata=" & slv_to_bin(outp.wrdata)
			& " zero=" & to_string(outp.zero)
			& lf
			severity error;
		end if;
	end procedure;

begin

	exec_inst : entity work.exec
		port map(
			clk				=> clk,
			reset			=> res_n,
			stall			=> inp.stall,
			flush			=> '0',

			-- from DEC
			op				=> inp.op,
			pc_in			=> inp.pc_in,

			-- to MEM
			pc_old_out		=> outp.pc_old_out, --out
			pc_new_out		=> outp.pc_new_out, --out
			aluresult		=> outp.aluresult, --out
			wrdata			=> outp.wrdata, --out
			zero			=> outp.zero, --out

			memop_in		=> inp.memop_in,
			memop_out		=> outp.memop_out, --out
			wbop_in			=> inp.wbop_in,
			wbop_out		=> outp.wbop_out, --out

			-- FWD
			exec_op			=> open, --exec_exec_op, --out
			reg_write_mem	=> (write => '0',
								reg => ZERO_REG,
								data => ZERO_DATA),
			reg_write_wr	=> (write => '0',
								reg => ZERO_REG,
								data => ZERO_DATA)
		);

	stimulus : process
		variable fstatus: file_open_status;
	begin
		file_open(fstatus, input_file, "testdata/input.txt", READ_MODE);

		wait until res_n = '1';
		timeout(1, CLK_PERIOD);

		while not endfile(input_file) loop
			inp <= read_next_input(input_file);
			timeout(1, CLK_PERIOD);
		end loop;

		wait;
	end process;

	output_checker : process
		variable fstatus: file_open_status;
		variable output_ref : OUTPUT;
	begin
		file_open(fstatus, output_ref_file, "testdata/output.txt", READ_MODE);

		wait until res_n = '1';
		timeout(1, CLK_PERIOD);

		while not endfile(output_ref_file) loop
			output_ref := read_next_output(output_ref_file);

			wait until falling_edge(clk);
			check_output(output_ref);
			wait until rising_edge(clk);
		end loop;

		wait;
	end process;

	generate_clk : process
	begin
		clk_generate(clk, CLK_PERIOD);
		wait;
	end process;

	generate_reset : process
	begin
		res_n <= '0';
		wait until rising_edge(clk);
		res_n <= '1';
		wait;
	end process;

end architecture;
