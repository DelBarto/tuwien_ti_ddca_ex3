library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_textio.all;

library std; -- for Printing
use std.textio.all;

use work.mem_pkg.all;
use work.op_pkg.all;
use work.core_pkg.all;
use work.tb_util_pkg.all;


entity tb is
end entity;

architecture bench of tb is

	constant CLK_PERIOD : time := 10 ns;

	signal clk : std_logic;
	signal res_n : std_logic := '0';

	subtype byte_type is std_logic_vector(BYTE_WIDTH-1 downto 0);
	constant EMPTY_BYTE : byte_type := (others => '0');
	type word_type is array(BYTEEN_WIDTH-1 downto 0) of byte_type;
	constant EMPTY_WORD : word_type := (others => EMPTY_BYTE);

	function get_byte(slv : in std_logic_vector; pos : in integer) return byte_type is
	begin
		return slv((pos+1)*BYTE_WIDTH-1  downto pos*BYTE_WIDTH);
	end function;

	function word_to_slv(word : in word_type) return std_logic_vector is
		variable slv : std_logic_vector(DATA_WIDTH-1 downto 0);
	begin
		for i in 0 to BYTEEN_WIDTH-1 loop
			slv((i+1)*BYTE_WIDTH-1  downto i*BYTE_WIDTH) := word(i);
		end loop;
		return slv;
	end function;


	function byte_reord (slv : std_logic_vector; pat : string) return std_logic_vector is
		variable ret : word_type := EMPTY_WORD;
		-- variable byte : byte_type := EMPTY_BYTE;
		variable j : integer;
	begin
		-- assert pat'length = 4 report "Length of pattern string must be 4 not " & to_string(pat'length);
		-- assert pat(BYTEEN_WIDTH) /= 'S' report "Sign extention can not be  on the first position: " & pat;

		for i in 0 to BYTEEN_WIDTH-1 loop
			-- byte := get_byte(slv, i);
			j := BYTEEN_WIDTH-i;
			-- report "pat(i="&to_string(i+1)&")="&to_string(pat(i+1)) & "   i="&to_string(i)&"   j="&to_string(j);
			case pat(j) is
				when '0' =>
					ret(i) := get_byte(slv, 0);
				when '1' =>
					ret(i) := get_byte(slv, 1);
				when '2' =>
					ret(i) := get_byte(slv, 2);
				when '3' =>
					ret(i) := get_byte(slv, 3);
				when 'N' =>
					ret(i) := (others => '0');
				when 'S' =>
					if i /= 0 then
						if ret(i-1)(BYTE_WIDTH-1) = '1' then
							ret(i) := (others => '1');
						else
							ret(i) := (others => '0');
						end if;
					end if;
				when 'X' =>
					ret(i) := (others => '-');
				when others =>
					-- assert true report to_string(pat(i)) & " is a not allowed.";
					null;
			end case;
		end loop;
		return word_to_slv(ret);
	end function;



begin


	stimulus : process
		variable slv : data_type
			:= "00110000000000001000000000011100";
	begin
		report "slv=" & to_string(slv);
		report "slv_reordered=" & to_string(byte_reord(slv,"SS10"));


		wait;
	end process;



	generate_clk : process
	begin
		clk_generate(clk, CLK_PERIOD);
		wait;
	end process;

	generate_reset : process
	begin
		res_n <= '0';
		wait until rising_edge(clk);
		res_n <= '1';
		wait;
	end process;

end architecture;
