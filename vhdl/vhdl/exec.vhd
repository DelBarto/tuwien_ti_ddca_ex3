library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.core_pkg.all;
use work.op_pkg.all;

entity exec is
	port (
		clk				: in  std_logic;
		reset			: in  std_logic;
		stall			: in  std_logic;
		flush			: in  std_logic;

		-- from DEC
		op				: in  exec_op_type;
		pc_in			: in  pc_type;

		-- to MEM
		pc_old_out		: out pc_type;
		pc_new_out		: out pc_type;
		aluresult		: out data_type;
		wrdata			: out data_type;
		zero			: out std_logic;

		memop_in		: in  mem_op_type;
		memop_out		: out mem_op_type;
		wbop_in			: in  wb_op_type;
		wbop_out		: out wb_op_type;

		-- to ctrl
		exec_op			: out exec_op_type;
		-- to FWD
		reg_write_mem	: in  reg_write_type;
		reg_write_wr	: in  reg_write_type
	);
end exec;

architecture rtl of exec is


	signal int_A : data_type;
	signal int_B : data_type;
	signal int_R : data_type;
	signal int_Z : std_logic;
	signal int_op : exec_op_type;
	signal int_pc_in : pc_type;
	signal int_memop_in : mem_op_type;
	signal int_wbop_in : wb_op_type;
	-- FWD ?????????????????????

begin

	alu_inst : entity work.alu
	port map(
		op => int_op.aluop, --alu_op_type
		A => int_A,
		B => int_B,
		R => int_R,
		Z => int_Z
	);


	-- input : process(clk, reset, flush)
	-- begin
	-- 	if reset = '0' then
	-- 		int_pc_in <= (others => '0');
	-- 		int_op			<= EXEC_NOP;
	-- 		int_memop_in	<= MEM_NOP;
	-- 		int_wbop_in	<= WB_NOP;
	--
	-- 	else
	-- 		if flush = '1' then
	-- 			int_pc_in		<= (others => '0');
	-- 			int_op			<= EXEC_NOP;
	-- 			int_memop_in	<= MEM_NOP;
	-- 			int_wbop_in	<= WB_NOP;
	-- 		end if;
	--
	-- 		if rising_edge(clk) and stall = '0' then
	-- 			int_pc_in		<= pc_in;
	-- 			int_op			<= op;
	-- 			int_memop_in	<= memop_in;
	-- 			int_wbop_in	<= wbop_in;
	-- 		end if;
	-- 	end if;
	-- end process;
	input : process(clk, reset, flush, stall)
	begin
		if reset = '0' then
			int_pc_in		<= (others => '0');
			int_op			<= EXEC_NOP;
			int_memop_in	<= MEM_NOP;
			int_wbop_in		<= WB_NOP;
		elsif flush = '1' then
			int_pc_in		<= (others => '0');
			int_op			<= EXEC_NOP;
			int_memop_in	<= MEM_NOP;
			int_wbop_in		<= WB_NOP;
		elsif rising_edge(clk) and stall = '0' then
			int_pc_in		<= pc_in;
			int_op			<= op;
			int_memop_in	<= memop_in;
			int_wbop_in		<= wbop_in;
		end if;
	end process;


	-- process needs to be reconsidered
	multiplex : process(all)
	begin

		exec_op		<= int_op;
		pc_old_out	<= int_pc_in;
		memop_out	<= int_memop_in;
		wbop_out	<= int_wbop_in;

		int_A <= int_op.readdata1;
		if int_op.alusrc2 = '0' or
			int_memop_in.mem.memread = '1' or
			int_memop_in.mem.memwrite = '1' then
				int_B <= int_op.imm;
		else
				int_B <= int_op.readdata2;
		end if;


		pc_new_out <= int_pc_in;
		if(int_op.alusrc3 = '1') then
			--calc new pc
			if (int_op.alusrc1 = '1' and int_op.alusrc2 = '1') or
				(int_op.alusrc1 = '0' and int_op.alusrc2 = '0') then
					pc_new_out <= std_logic_vector(unsigned(int_pc_in) +
						unsigned(int_op.imm(PC_WIDTH-1 downto 1) & '0'));
			end if;
		end if;
		aluresult <= int_R;
		zero <= int_Z;
		wrdata <= int_op.readdata2;

	end process;

end architecture;
