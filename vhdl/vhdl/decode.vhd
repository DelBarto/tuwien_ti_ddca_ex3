library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.core_pkg.all;
use work.op_pkg.all;

entity decode is
	port (
		clk, reset : in  std_logic;
		stall      : in  std_logic;
		flush      : in  std_logic;

		-- from fetch
		pc_in      : in  pc_type;
		instr      : in  instr_type;

		-- from writeback
		reg_write  : in reg_write_type;

		-- towards next stages
		pc_out     : out pc_type;
		exec_op    : out exec_op_type;
		mem_op     : out mem_op_type;
		wb_op      : out wb_op_type;

		-- exceptions
		exc_dec    : out std_logic
	);
end entity;

architecture rtl of decode is

	signal int_instr : instr_type;
	signal int_pc : pc_type;
	signal int_rddata1 : instr_type;
	signal int_rddata2 : instr_type;

	constant OPC_LOAD	: std_logic_vector(6 downto 0) := "0000011";
	constant OPC_STORE	: std_logic_vector(6 downto 0) := "0100011";
	constant OPC_BRANCH	: std_logic_vector(6 downto 0) := "1100011";
	constant OPC_JALR	: std_logic_vector(6 downto 0) := "1100111";
	constant OPC_JAL	: std_logic_vector(6 downto 0) := "1101111";
	constant OPC_OP_IMM	: std_logic_vector(6 downto 0) := "0010011";
	constant OPC_OP		: std_logic_vector(6 downto 0) := "0110011";
	constant OPC_AUIPC	: std_logic_vector(6 downto 0) := "0010111";
	constant OPC_LUI	: std_logic_vector(6 downto 0) := "0110111";
	constant OPC_NOP	: std_logic_vector(6 downto 0) := "0001111";

	alias opcode	: std_logic_vector(6 downto 0) is int_instr(6 downto 0);
	alias rd		: std_logic_vector(4 downto 0) is int_instr(11 downto 7);
	alias funct3	: std_logic_vector(2 downto 0) is int_instr(14 downto 12);
	alias rs1		: std_logic_vector(4 downto 0) is int_instr(19 downto 15);
	alias rs2		: std_logic_vector(4 downto 0) is int_instr(24 downto 20);
	alias funct7	: std_logic_vector(6 downto 0) is int_instr(31 downto 25);

	constant F3_JALR	: std_logic_vector(2 downto 0) := "000";

	constant F3_BEQ		: std_logic_vector(2 downto 0) := "000";
	constant F3_BNE		: std_logic_vector(2 downto 0) := "001";
	constant F3_BLT		: std_logic_vector(2 downto 0) := "100";
	constant F3_BGE		: std_logic_vector(2 downto 0) := "101";
	constant F3_BLTU	: std_logic_vector(2 downto 0) := "110";
	constant F3_BGEU	: std_logic_vector(2 downto 0) := "111";

	constant F3_LB		: std_logic_vector(2 downto 0) := "000";
	constant F3_LH		: std_logic_vector(2 downto 0) := "001";
	constant F3_LW		: std_logic_vector(2 downto 0) := "010";
	constant F3_LBU		: std_logic_vector(2 downto 0) := "100";
	constant F3_LHU		: std_logic_vector(2 downto 0) := "101";

	constant F3_SB		: std_logic_vector(2 downto 0) := "000";
	constant F3_SH		: std_logic_vector(2 downto 0) := "001";
	constant F3_SW		: std_logic_vector(2 downto 0) := "010";

	constant F3_ADDI	: std_logic_vector(2 downto 0) := "000";
	constant F3_SLTI	: std_logic_vector(2 downto 0) := "010";
	constant F3_SLTIU	: std_logic_vector(2 downto 0) := "011";
	constant F3_XORI	: std_logic_vector(2 downto 0) := "100";
	constant F3_ORI		: std_logic_vector(2 downto 0) := "110";
	constant F3_ANDI	: std_logic_vector(2 downto 0) := "111";
	constant F3_SLLI	: std_logic_vector(2 downto 0) := "001";
	constant F3_SRLI	: std_logic_vector(2 downto 0) := "101";
	constant F3_SRAI	: std_logic_vector(2 downto 0) := "101";

	constant F3_ADD		: std_logic_vector(2 downto 0) := "000";
	constant F3_SUB		: std_logic_vector(2 downto 0) := "000";
	constant F3_SLL		: std_logic_vector(2 downto 0) := "001";
	constant F3_SLT		: std_logic_vector(2 downto 0) := "010";
	constant F3_SLTU	: std_logic_vector(2 downto 0) := "011";
	constant F3_XOR		: std_logic_vector(2 downto 0) := "100";
	constant F3_SRL		: std_logic_vector(2 downto 0) := "101";
	constant F3_SRA		: std_logic_vector(2 downto 0) := "101";
	constant F3_OR		: std_logic_vector(2 downto 0) := "110";
	constant F3_AND		: std_logic_vector(2 downto 0) := "111";

	constant F3_FENCE	: std_logic_vector(2 downto 0) := "000";

	constant F7_ADD, F7_SLL, F7_SLT, F7_SLTU, F7_XOR, F7_SRL, F7_OR, F7_AND
				: std_logic_vector(6 downto 0) := (others=>'0');

	constant F7_SUB : std_logic_vector(6 downto 0) := "0100000";
	constant F7_SRA : std_logic_vector(6 downto 0) := "0100000";

begin

	regfile_inst : entity work.regfile
	port map(
		clk			=> clk,
		reset		=> reset,
		stall		=> stall,
		rdaddr1		=> instr(19 downto 15), -- 4 downto 0
		rdaddr2		=> instr(24 downto 20), -- 4 downto 0
		rddata1		=> int_rddata1, -- out
		rddata2		=> int_rddata2, -- out
		wraddr		=> reg_write.reg,
		wrdata		=> reg_write.data,
		regwrite	=> reg_write.write
	);

	-- read : process(clk, reset, flush, stall)
	-- begin
	-- 	if  reset = '0' then
	-- 		internal_instr <= (others => '0');
	-- 	else
	-- 		if flush = '1' then
	-- 			internal_instr	<= (others => '0');
	-- 		end if;
	--
	-- 		if rising_edge(clk) and stall = '0' then
	-- 			internal_instr	<= instr;
	-- 			pc_out			<= pc_in;
	-- 		end if;
	-- 	end if;
	-- end process;


	read : process(clk, reset, flush, stall)
	begin
		if reset = '0' then
			int_instr	<= (others => '0');
			pc_out		<= (others => '0');
		elsif flush = '1' then
			int_instr	<= NOP_INST;
			pc_out		<= (others => '0');
		elsif rising_edge(clk) and stall = '0' then
			-- int_instr <= int_instr_next;
			-- if stall = '0' then
				int_instr	<= instr;
				pc_out		<= pc_in;
			-- end if;
		end if;
	end process;



	decode_output : process(all)
	begin
		-- int_instr_next <= int_instr;
		exc_dec <= '0';
		exec_op <= EXEC_NOP;
		mem_op <= MEM_NOP;
		wb_op <= WB_NOP;

		case int_instr(6 downto 0) is
			when OPC_LOAD	 => -- I
				-- [31  imm[11:0]  20][19  rs1  15][14  funct3  12]
				-- [11  rd  7][6  opcode  0]
				exec_op.aluop <= ALU_ADD;
				exec_op.rs1 <= rs1;
				exec_op.readdata1 <= int_rddata1;
				exec_op.alusrc1 <= '1';
				exec_op.imm <= (others => int_instr(31));
				exec_op.imm(10 downto 0) <= int_instr(30 downto 20);

				mem_op.mem.memread <= '1';

				wb_op.rd <= rd;
				wb_op.write <= '1';
				wb_op.src <= WBS_MEM;
				case funct3 is
					when F3_LB	=> -- LB rd,rs1,imm
						mem_op.mem.memtype <= MEM_B;
					when F3_LH	=> --LH rd,rs1,imm
						mem_op.mem.memtype <= MEM_H;
					when F3_LW	=> -- LW rd,rs1,imm
						mem_op.mem.memtype <= MEM_W;
					when F3_LBU	=> -- LBU rd,rs1,imm
						mem_op.mem.memtype <= MEM_BU;
					when F3_LHU	=> -- LHU rd,rs1,imm
						mem_op.mem.memtype <= MEM_HU;
					when others	=>
						exc_dec <= '1';
				end case;


			when OPC_STORE	=> -- S
				-- [31  imm[11:5]  25][24  rs2  20][19  rs1  15]
				-- [14  funct3  12][11  rd  7][6  opcode  0]
				exec_op.aluop <= ALU_ADD;
				exec_op.rs1 <= rs1;
				exec_op.rs2 <= rs2;
				exec_op.readdata1 <= int_rddata1;
				exec_op.readdata2 <= int_rddata2;
				exec_op.alusrc1 <= '1';
				exec_op.alusrc2 <= '1';
				exec_op.imm <= (others => int_instr(31));
				exec_op.imm(10 downto 0) <=
					int_instr(30 downto 25) &
					int_instr(11 downto  7) ;

				mem_op.mem.memwrite <= '1';
				case funct3 is
					when F3_SB	=> -- SB rs1,rs2,imm
						mem_op.mem.memtype <= MEM_B;
					when F3_SH	=> -- SH rs1,rs2,imm
						mem_op.mem.memtype <= MEM_H;
					when F3_SW	=> -- SW rs1,rs2,imm
						mem_op.mem.memtype <= MEM_W;
					when others	=>
						exc_dec <= '1';
				end case;

			when OPC_BRANCH	=> -- B
				-- [31  imm[12]  31][30  imm[10:5]  25][24  rs2  20]
				-- [19  rs1  15][14  funct3  12][11  imm[4:1]  8]
				-- [7  imm[11]  7][6  opcode  0]
				exec_op.alusrc1 <= '1';
				exec_op.alusrc2 <= '1';
				exec_op.alusrc3 <= '1';
				exec_op.imm <= (others => int_instr(31));
				exec_op.imm(11 downto 0) <=
					int_instr(7) & int_instr(30 downto 25) &
					int_instr(11 downto 8) & '0';
				exec_op.rs1 <= rs1;
				exec_op.readdata1 <= int_rddata1;
				exec_op.rs2 <= rs2;
				exec_op.readdata2 <= int_rddata2;

				case funct3 is
					when F3_BEQ		=> -- BEQ rs1,rs2,imm
						exec_op.aluop <= ALU_SUB; --???
						mem_op.branch <= BR_CND;
					when F3_BNE		=> -- BNE rs1,rs2,imm
						exec_op.aluop <= ALU_SUB; --???
						mem_op.branch <= BR_CNDI;
					when F3_BLT		=> -- BLT rs1,rs2,imm
						exec_op.aluop <= ALU_SLT;
						mem_op.branch <= BR_CND;
					when F3_BGE		=> -- BGE rs1,rs2,imm
						exec_op.aluop <= ALU_SLT;
						mem_op.branch <= BR_CNDI;
					when F3_BLTU	=> -- BLTU rs1,rs2,imm
						exec_op.aluop <= ALU_SLTU;
						mem_op.branch <= BR_CND;
					when F3_BGEU	=> -- BGEU rs1,rs2,imm
						exec_op.aluop <= ALU_SLTU;
						mem_op.branch <= BR_CNDI;
					when others	=>
						exc_dec <= '1';
				end case;

			when OPC_JALR	=> -- I - JALR rd,rs1,imm
				-- [31  imm[11:0]  20][19  rs1  15][14  funct3  12]
				-- [11  rd  7][6  opcode  0]
				exec_op.aluop <= ALU_ADD;
				exec_op.rs1 <= rs1;
				exec_op.readdata1 <= int_rddata1;
				exec_op.alusrc3 <= '1';

				exec_op.imm <= (others => int_instr(31));
				exec_op.imm(10 downto 0) <= int_instr(30 downto 20);

				mem_op.branch <= BR_BR;
				wb_op.rd <= rd;
				wb_op.src <= WBS_OPC;
				wb_op.write <= '1';

			when OPC_JAL	=> -- J - JAL rd, imm
				-- [31  imm[20]  31][30  imm[10:1]  21][20  imm[11]  20]
				-- [19  imm[19:12]  12][11  rd  7][6  opcode  0]
				exec_op.aluop <= ALU_ADD;
				exec_op.rs1 <= rs1;
				exec_op.readdata1 <= int_rddata1;
				exec_op.alusrc3 <= '1';

				exec_op.imm <= (others => int_instr(31));
				exec_op.imm(19 downto 0) <=
					int_instr(19 downto 12) & int_instr(20) &
					int_instr(30 downto 21) & '0';

				mem_op.branch	<= BR_BR;
				wb_op.rd		<= rd;
				wb_op.src		<= WBS_OPC;
				wb_op.write		<= '1';

			when OPC_OP_IMM	=> -- I
				-- [31  imm[11:0]  20][19  rs1  15][14  funct3  12]
				-- [11  rd  7][6  opcode  0]
				exec_op.alusrc1 <= '1';
				exec_op.rs1 <= rs1;
				exec_op.readdata1 <= int_rddata1;
				exec_op.imm <= (others => int_instr(31));
				exec_op.imm(10 downto 0) <= int_instr(30 downto 20);

				wb_op.rd <= rd;
				wb_op.src <= WBS_ALU;
				wb_op.write <= '1';
				case funct3 is
					when F3_ADDI	=>
						exec_op.aluop <= ALU_ADD;
					when F3_SLTI	=>
						exec_op.aluop <= ALU_SLT;
					when F3_SLTIU	=>
						exec_op.aluop <= ALU_SLTU;
					when F3_XORI	=>
						exec_op.aluop <= ALU_XOR;
					when F3_ORI		=>
						exec_op.aluop <= ALU_OR;
					when F3_ANDI	=>
						exec_op.aluop <= ALU_AND;
					when F3_SLLI	=>
						exec_op.aluop <= ALU_SLL;
					-- when F3_SRLI	=> -- equivalent to SRAI
					-- 	exec_op.aluop <= ALU_SRL;
					-- when F3_SRAI	=>
					-- 	exec_op.aluop <= ALU_SRA;
					when F3_SRLI	=> -- F3_SRLI == F3_SRAI
						if int_instr(30) = '0' then -- is equal to imm[10]
							exec_op.aluop <= ALU_SRL;
						else
							exec_op.aluop <= ALU_SRA;
						end if;

					when others	=>
						exc_dec <= '1';
				end case;

			when OPC_OP		=> -- R
				-- [31 funct7  25][24  rs2  20][19  rs1  15]
				-- [14  funct3  12][11  rd  7][6  opcode  0]
				exec_op.rs1 <= rs1;
				exec_op.rs2 <= rs2;
				exec_op.readdata1 <= int_rddata1;
				exec_op.readdata2 <= int_rddata2;
				exec_op.alusrc1 <= '1';
				exec_op.alusrc2 <= '1';
				-- mem_op
				wb_op.rd <= rd;
				wb_op.src <= WBS_ALU;
				wb_op.write <= '1';
				case( funct3 &  funct7) is
					when F3_ADD		& F7_ADD		=>
						exec_op.aluop <= ALU_ADD;
					when F3_SUB		& F7_SUB		=>
						exec_op.aluop <= ALU_SUB;
					when F3_SLL		& F7_SLL		=>
						exec_op.aluop <= ALU_SLL;
					when F3_SLT		& F7_SLT		=>
						exec_op.aluop <= ALU_SLT;
					when F3_SLTU	& F7_SLTU		=>
						exec_op.aluop <= ALU_SLTU;
					when F3_XOR		& F7_XOR		=>
						exec_op.aluop <= ALU_XOR;
					when F3_SRL		& F7_SRL		=>
						exec_op.aluop <= ALU_SRL;
					when F3_SRA		& F7_SRA		=>
						exec_op.aluop <= ALU_SRA;
					when F3_OR		& F7_OR			=>
						exec_op.aluop <= ALU_OR;
					when F3_AND		& F7_AND		=>
						exec_op.aluop <= ALU_AND;
					when others =>
						exc_dec <= '1';
				end case;

			when OPC_AUIPC	=> -- U
				-- [31 imm[31:12]  12][11  rd  7][6  opcode  0]
				exec_op.aluop <= ALU_SLL;
				exec_op.alusrc3 <= '1'; --???????
				exec_op.imm <= (others => '0');
				exec_op.imm(31 downto 12) <= int_instr(31 downto 12);

				wb_op.rd <= rd;
				wb_op.src <= WBS_OPC;
				wb_op.write <= '1';
			when OPC_LUI	=> -- U
				-- [31 imm[31:12]  12][11  rd  7][6  opcode  0]
				exec_op.aluop <= ALU_SLL;
				exec_op.alusrc3 <= '1'; --???????
				exec_op.imm <= (others => '0');
				exec_op.imm(31 downto 12) <= int_instr(31 downto 12);

				wb_op.rd <= rd;
				wb_op.src <= WBS_OPC;
				wb_op.write <= '1';

			when OPC_NOP	=> -- I
				-- [31  imm[11:0]  20][19  rs1  15][14  funct3  12]
				-- [11  rd  7][6  opcode  0]
				if funct3 = "000" then
					-- nop
					exec_op.alusrc1 <= '1';
					exec_op.rs1 <= rs1;
					exec_op.readdata1 <= int_rddata1;
					exec_op.imm <= (others => int_instr(31));
					exec_op.imm(10 downto 0) <= int_instr(30 downto 20);
					-- wb_op.rd <= rd;
					-- wb_op.src <= WBS_ALU;
					-- wb_op.write <= '1';
				else
					exc_dec <= '1';
				end if;
			when others =>
				exc_dec <= '1';
		end case;


	end process;

end architecture;
