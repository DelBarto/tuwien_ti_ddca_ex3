library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library std;
use std.textio.all;
use ieee.std_logic_textio.all;

use work.mem_pkg.all;
use work.core_pkg.all;
use work.op_pkg.all;

entity memu is
	port (
		-- to mem
		op   : in  memu_op_type;
		A	: in  data_type;
		W	: in  data_type;
		R	: out data_type := (others => '0');

		B	: out std_logic := '0';
		XL   : out std_logic := '0';
		XS   : out std_logic := '0';

		-- to memory controller
		D	: in  mem_in_type;
		M	: out mem_out_type --:= MEM_OUT_NOP
	);
end memu;

architecture rtl of memu is

	subtype byte_type is std_logic_vector(BYTE_WIDTH-1 downto 0);
	constant EMPTY_BYTE : byte_type := (others => '0');
	type word_type is array(BYTEEN_WIDTH-1 downto 0) of byte_type;
	constant EMPTY_WORD : word_type := (others => EMPTY_BYTE);

	function get_byte(slv : in std_logic_vector; pos : in integer) return byte_type is
	begin
		return slv((pos+1)*BYTE_WIDTH-1  downto pos*BYTE_WIDTH);
	end function;

	function word_to_slv(word : in word_type) return std_logic_vector is
		variable slv : std_logic_vector(DATA_WIDTH-1 downto 0);
	begin
		for i in 0 to BYTEEN_WIDTH-1 loop
			slv((i+1)*BYTE_WIDTH-1  downto i*BYTE_WIDTH) := word(i);
		end loop;
		return slv;
	end function;


	function byte_reord (slv : std_logic_vector; pat : string) return std_logic_vector is
		variable ret : word_type := EMPTY_WORD;
		-- variable byte : byte_type := EMPTY_BYTE;
		variable j : integer;
	begin
		-- assert pat'length = 4 report "Length of pattern string must be 4 not " & to_string(pat'length);
		-- assert pat(BYTEEN_WIDTH) /= 'S' report "Sign extention can not be  on the first position: " & pat;


		for i in 0 to BYTEEN_WIDTH-1 loop
			-- byte := get_byte(slv, i);
			j := BYTEEN_WIDTH-i;
			case pat(j) is
				when '0' =>
					ret(i) := get_byte(slv, 0);
				when '1' =>
					ret(i) := get_byte(slv, 1);
				when '2' =>
					ret(i) := get_byte(slv, 2);
				when '3' =>
					ret(i) := get_byte(slv, 3);
				when 'N' =>
					ret(i) := (others => '0');
				when 'S' =>
					if i /= 0 then
						if ret(i-1)(BYTE_WIDTH-1) = '1' then
							ret(i) := (others => '1');
						else
							ret(i) := (others => '0');
						end if;
					end if;
				when 'X' =>
					ret(i) := (others => '-');
				when others =>
					-- assert true report to_string(pat(i)) & " is a not allowed.";
					null;
			end case;
		end loop;
		return word_to_slv(ret);
	end function;

begin


	-- M.rd	<= '0' when (XL or XS) = '1' else op.memread;
	-- M.wr	<= '0' when (XL or XS) = '1' else op.memwrite;
	-- B		<= '1' when ((D.busy or M.rd) = '1') and XL = '0' else '0';

	compute : process(all)
		variable M_rd : std_logic;
		variable M_wr : std_logic;
		variable XL_temp : std_logic;
		variable XS_temp : std_logic;
	begin
		M.address <= A(PC_WIDTH-1 downto 2);
		-- B <= '0';

	-- compute M.byteena and M.wrdata
		case op.memtype is
			when MEM_B | MEM_BU =>
				case A(1 downto 0) is
					when "00" =>
						M.byteena <= "1000";
						M.wrdata <= byte_reord(W,"0XXX");
					when "01" =>
						M.byteena <= "0100";
						M.wrdata <= byte_reord(W,"X0XX");
					when "10" =>
						M.byteena <= "0010";
						M.wrdata <= byte_reord(W,"XX0X");
					when "11" =>
						M.byteena <= "0001";
						M.wrdata <= byte_reord(W,"XXX0");
					when others =>
						null;
				end case;

			when MEM_H | MEM_HU =>
				case A(1 downto 0) is
					when "00" | "01" =>
						M.byteena <= "1100";
						M.wrdata <= byte_reord(W,"01XX");
					when "10" | "11" =>
						M.byteena <= "0011";
						M.wrdata <= byte_reord(W,"XX01");
					when others =>
						null;
				end case;

			when MEM_W =>
				M.byteena <= "1111";
				M.wrdata <= byte_reord(W, "0123");

			when others =>
				--shouldn't occure
				null;
		end case;


		-- compute exeptions
		XL_temp := '0';
		XS_temp := '0';

		if op.memtype = MEM_H or op.memtype = MEM_HU then
			if A(1 downto 0) = "01" or A(1 downto 0) =  "11" then
				if op.memread = '1' then
					XL_temp := '1';
					XS_temp := '0';
				elsif op.memwrite = '1' then
					XL_temp := '0';
					XS_temp := '1';
				end if;
			end if;
		elsif op.memtype = MEM_W then
			if A(1 downto 0) = "10" or
				A(1 downto 0) = "10" or
				A(1 downto 0) = "11" then
					if op.memread = '1' then
						XL_temp := '1';
						XS_temp := '0';
					elsif op.memwrite = '1' then
						XL_temp := '0';
						XS_temp := '1';
					end if;
			end if;
		end if;

		M_rd	:= op.memread;
		M_wr	:= op.memwrite;
		if (XL_temp or XS_temp) = '1' then
			M_rd	:= '0';
			M_wr	:= '0';
		end if;

		M.rd <= M_rd;
		M.wr <= M_wr;
		XL <= XL_temp;
		XS <= XS_temp;

		B	<= '0';
		if ((D.busy or M_rd) = '1') and XL_temp = '0' then
			B <= '1';
		end if;



	-- compute R
		case op.memtype is
			when MEM_B =>
				case A(1 downto 0) is
					when "00" =>
						R <= byte_reord(D.rddata, "SSS3");
					when "01" =>
						R <= byte_reord(D.rddata, "SSS2");
					when "10" =>
						R <= byte_reord(D.rddata, "SSS1");
					when "11" =>
						R <= byte_reord(D.rddata, "SSS0");
					when others =>
						null;
				end case;

			when MEM_BU =>
				case A(1 downto 0) is
					when "00" =>
						R <= byte_reord(D.rddata, "NNN3");
					when "01" =>
						R <= byte_reord(D.rddata, "NNN2");
					when "10" =>
						R <= byte_reord(D.rddata, "NNN1");
					when "11" =>
						R <= byte_reord(D.rddata, "NNN0");
					when others =>
						null;
				end case;

			when MEM_H =>
				case A(1 downto 0) is
					when "00" | "01" =>
						R <= byte_reord(D.rddata, "SS23");
					when "10" | "11" =>
						R <= byte_reord(D.rddata, "SS01");
					when others =>
						null;
				end case;

			when MEM_HU =>
				case A(1 downto 0) is
					when "00" | "01" =>
						R <= byte_reord(D.rddata, "NN23");
					when "10" | "11" =>
						R <= byte_reord(D.rddata, "NN01");
					when others =>
						null;
				end case;

			when MEM_W =>
				R <= byte_reord(D.rddata, "0123");

			when others =>
				-- should't occure
				null;
		end case;


	end process;

end architecture;

